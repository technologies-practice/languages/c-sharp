﻿using CSharpPractice;
using System;
using Xunit;

namespace CSharpPracticeTest
{
    public class FactorialCalculatorTest
    {
        [Theory]
        [InlineData(0, 1)]
        [InlineData(1, 1)]
        [InlineData(2, 2)]
        [InlineData(5, 120)]
        public void ShouldCalculateCorrectly(int source, long expected)
        {
            var actual = FactorialCalculator.CalculateFrom(source);
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(-100)]
        [InlineData(-200)]
        [InlineData(-999)]
        public void ShouldThrowsExceptionWhenNegativeSource(int source)
        {
            _ = Assert.Throws<InvalidOperationException>(() => FactorialCalculator.CalculateFrom(source));
        }
    }
}
