﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace CSharpPractice
{
    public class FactorialCalculator
    {
        public static long CalculateFrom(int source)
        {
            if (source < 0)
            {
                throw new InvalidOperationException("The factorial of negative numbers is not defined");
            }

            return CalculteFactorial(source);

        }

        private static long CalculteFactorial(int source)
        {
            return source == 1 || source == 0
                ? 1
                : source * CalculateFrom(source - 1);
        }
    }
}
